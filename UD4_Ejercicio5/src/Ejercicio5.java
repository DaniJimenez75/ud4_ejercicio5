/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int A = 8;
		int B = 4;
		int C = 9;
		int D = 12;
		
		System.out.println("El valor inical de las variables es:");
		System.out.println("A:"+A);
		System.out.println("B:"+B);
		System.out.println("C:"+C);
		System.out.println("D:"+D);

		B = C;
		C = A;
		A = D;
		D = B;
		
		System.out.println("El nuevo valor de las variables es:");
		System.out.println("A:"+A);
		System.out.println("B:"+B);
		System.out.println("C:"+C);
		System.out.println("D:"+D);

	}

}
